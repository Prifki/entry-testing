function fibonacci(N) {
	if (!isNaN(parseFloat(N)) && isFinite(N) && ((N ^ 0) === N) && N > 0) {
		var a = 1, b = 0, c;
		for (var i = 0; i < N; i++) {
			c = a + b;
			a = b;
			b = c;
		}
		return c;
    }
	else
		return 'N is not correct';
}
fibonacci(+prompt('Input N'));